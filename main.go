package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var TG_BOT_TOKEN = os.Getenv("TG_BOT_TOKEN")
var TG_USER_ID = os.Getenv("TG_USER_ID")
var JUKEBOT_DB_PATH = os.Getenv("JUKEBOT_DB_PATH")

const MAP_FILE_NAME = "map.json"

func readMapFile(path string) (map[string]string, error) {
	// Open the file
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Read the file
	mapFile, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	mapFileBytes := []byte(mapFile)

	// Convert a json file of a map to a map
	var mapFileMap map[string]string
	err = json.Unmarshal(mapFileBytes, &mapFileMap)
	if err != nil {
		return nil, err
	}

	return mapFileMap, nil
}

func writeMapFile(mapFileMap map[string]string, path string) error {
	// Convert a map to a json file of a map
	mapFileBytes, err := json.Marshal(mapFileMap)
	if err != nil {
		return err
	}

	// Write the file
	err = ioutil.WriteFile(path, mapFileBytes, 0644)
	if err != nil {
		return err
	}

	return nil
}

// called on init to ensure that the map file exists
func initDir(mapFilePath string) error {

	if err := os.MkdirAll(filepath.Dir(mapFilePath), 0755); err != nil {
		log.Fatal(err)
	}

	if _, err := os.Stat(mapFilePath); os.IsNotExist(err) {
		if err = ioutil.WriteFile(mapFilePath, []byte("{}"), 0644); err != nil {
			log.Fatal(err)
		}
	}
	return nil
}

type Server struct {
	BotAPI      *tgbotapi.BotAPI
	ChatId      int64
	MapFilePath string
	LinkMap     map[string]string
}

func (s *Server) Init() error {
	parsedChatID, err := strconv.ParseInt(TG_USER_ID, 10, 64)
	if err != nil {
		return err
	}

	s.ChatId = parsedChatID

	s.MapFilePath = filepath.Join(JUKEBOT_DB_PATH, MAP_FILE_NAME)

	if err := initDir(s.MapFilePath); err != nil {
		return err
	}
	if err := s.loadMap(); err != nil {
		return err
	}

	bot, err := tgbotapi.NewBotAPI(TG_BOT_TOKEN)
	if err != nil {
		return err
	}

	bot.Debug = true

	s.BotAPI = bot

	return nil
}

func (s *Server) loadMap() error {
	fmt.Println("reading from: ", s.MapFilePath)
	linkMap, err := readMapFile(s.MapFilePath)
	if err != nil {
		return err
	}
	s.LinkMap = linkMap
	return nil
}

func (s *Server) saveMap() error {
	if err := writeMapFile(s.LinkMap, s.MapFilePath); err != nil {
		return err
	}
	return nil
}

func (s *Server) ProcessInput() {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	fmt.Println("Starting to poll for updates")
	updates := (*s.BotAPI).GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		if update.Message.From.ID == s.ChatId {
			cmd := strings.Split(update.Message.Text, " ")

			switch cmd[0] {
			case "/add":
				mnemonic, link := cmd[1], cmd[2]
				s.LinkMap[mnemonic] = link

				writeErr := s.saveMap()
				if writeErr != nil {
					log.Fatal(writeErr)
				}

				text := fmt.Sprintf("Added '%s' as '%s' to your stored audio.", link, mnemonic)

				msg := tgbotapi.NewMessage(update.Message.Chat.ID, text)
				msg.ReplyToMessageID = update.Message.MessageID

				s.BotAPI.Send(msg)
			case "/playtrack":
				var mnemonic string
				var text string
				if len(cmd) > 1 {
					mnemonic = cmd[1]
				} else {
					rand.Seed(time.Now().UnixNano())
					keys := make([]string, 0, len(s.LinkMap))
					for k := range s.LinkMap {
						keys = append(keys, k)
					}
					mnemonic = keys[rand.Intn(len(keys))]
				}

				link, ok := s.LinkMap[mnemonic]
				if ok {
					text = fmt.Sprintf("Playing '%s' from '%s'", mnemonic, link)
				} else {
					text = fmt.Sprintf("No track found for '%s'", mnemonic)
				}

				msg := tgbotapi.NewMessage(update.Message.Chat.ID, text)
				msg.ReplyToMessageID = update.Message.MessageID

				s.BotAPI.Send(msg)

			default:
				text := "I don't understand you, sorry."

				msg := tgbotapi.NewMessage(update.Message.Chat.ID, text)
				msg.ReplyToMessageID = update.Message.MessageID

				s.BotAPI.Send(msg)
			}

		}
	}
}

func main() {
	var s Server

	if err := s.Init(); err != nil {
		errMsg := fmt.Sprintf("Error initializing server: %s", err)
		log.Fatal(errMsg)
	}

	s.ProcessInput()
}

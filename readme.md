# Jukebot
> Customized JukeBox served over telegram

## Install
Clone this repo and set the following environment variables in your shell:
```
export TG_BOT_TOKEN=YOUR_TELEGRAM_BOT_TOKEN
export TG_USER_ID=YOUR_TELEGRAM_USERID
```

Resource your shell, and when you echo the following environment variables you should see a response:
```
$ echo $TG_BOT_TOKEN
YOUR_TELEGRAM_BOT_TOKEN

$ echo $TG_USER_ID
YOUR_TELEGRAM_USERID
```

The latter is harder to obtain, as you need to first create a chat in telegram to your bot and make an api call to the telegram api in order
to see your user id.

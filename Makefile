EXE_NAME=jukebot

.PHONY: all
all: build

.PHONY: build
build:
	go build -o $(EXE_NAME) .
